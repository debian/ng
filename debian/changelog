ng (1.5~beta1-13) unstable; urgency=medium

  * QA upload.

  [ Kei Hibino ]
  * Support terminals with more than 127 lines.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 12 Feb 2025 19:03:26 +0100

ng (1.5~beta1-12) unstable; urgency=medium

  * QA upload.
  * Fix building with -Werror=implicit-int.  (Closes: #1075315)
  * Enable all hardening flags.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 22 Aug 2024 15:49:35 +0200

ng (1.5~beta1-11) unstable; urgency=medium

  * QA upload.
  * Fix building with -Werror=implicit-function-declaration.
    (Closes: #1066688)

 -- Andreas Beckmann <anbe@debian.org>  Thu, 11 Apr 2024 17:24:49 +0200

ng (1.5~beta1-10) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.5.0, no changes needed.

  [ Simon McVittie ]
  * d/rules: Specify canonical paths of cp, ls, mv, rmdir
    (Closes: #993275)

  [ Vagrant Cascadian ]
  * debian/control: Set Rules-Requires-Root to "no".
  * debian/control: Update Standards-Version to 4.6.0.
  * Switch to debhelper-compat 13 and dh.
  * debian/rules: Remove boilerplate comments.
  * Use dh_install to install ng binaries and documentation.
  * Remove debian/*.dirs files as they are no longer necessary.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 28 Sep 2021 15:13:46 -0700

ng (1.5~beta1-9) unstable; urgency=medium

  * QA upload.
  * Bumped Standards-Version to 4.4.1.

 -- Masayuki Hatta <mhatta@debian.org>  Sat, 28 Dec 2019 16:26:23 +0100

ng (1.5~beta1-8) unstable; urgency=medium

  * QA upload.
  * Re-upload as source only to target testing.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 01 Sep 2019 14:34:37 -0300

ng (1.5~beta1-7) unstable; urgency=medium

  * QA upload.
  * undo.c: Fixed a typo.

 -- Masayuki Hatta <mhatta@debian.org>  Tue, 20 Aug 2019 18:08:01 -0700

ng (1.5~beta1-6) unstable; urgency=medium

  * QA upload.
  * Fix FTCBFS: Let dpkg's buildtools.mk supply a CC, thanks to Helmut Grohne
    - closes: #934968
  * Add manpage - closes: #850638

 -- Masayuki Hatta <mhatta@debian.org>  Tue, 20 Aug 2019 16:26:19 -0700

ng (1.5~beta1-5) unstable; urgency=medium

  * QA upload.
  * Bumped to Standards-Version: 4.4.0.
  * d/rules: Replaced dh_autotools-dev_* with dh_update_autotools_config.
  * d/p/utf8-support.patch:  Added UTF-8 support.

 -- Masayuki Hatta <mhatta@debian.org>  Wed, 14 Aug 2019 17:08:41 -0700

ng (1.5~beta1-4) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group.  (See: #836476)
  * Switch to debhelper compat level 10.  (Closes: #817593, #822010)
  * Switch to source format 3.0 (quilt).
  * Fix FTBFS with clang, thanks to Nicolas Sévelin-Radiguet.
    (Closes: #760672)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 12 Dec 2016 11:01:12 +0100

ng (1.5~beta1-3.1) unstable; urgency=medium

  [ Aurelien Jarno ]
  * Non-maintainer upload.

  [ Logan Rosen ]
  * Use autotools-dev to update config.{sub,guess} for new arches. Closes:
    #565180.

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 22 Sep 2014 20:46:44 +0200

ng (1.5~beta1-3) unstable; urgency=low

  * Bumped to Standards-Version: 3.8.0.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Thu, 17 Jul 2008 08:10:34 +0900

ng (1.5~beta1-2) unstable; urgency=low

  * debian/rules: Inserted a space before "]".

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Fri, 07 Dec 2007 06:02:57 +0900

ng (1.5~beta1-1) unstable; urgency=low

  * New upstream release.
  * Bumped to Standards-Version: 3.7.3.
  * Fixed various lintian warnings.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Thu, 06 Dec 2007 20:54:11 +0900

ng (1.4.4p1-4) unstable; urgency=low

  * Bumped to Standards-Version: 3.7.2.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Fri, 13 Oct 2006 22:53:35 +0900

ng (1.4.4p1-3) unstable; urgency=low

  * [control] changed Maintainer field.
  * Bumped to Standards-Version: 3.6.1.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Wed, 18 Feb 2004 01:44:10 +0900

ng (1.4.4p1-2) unstable; urgency=low

  * Added Build-Depends: libncurses-dev - closes: #201457
  * Removed periods from the short description - closes: #201686

 -- Masayuki Hatta <mhatta@debian.org>  Sun, 27 Jul 2003 01:02:09 +0900

ng (1.4.4p1-1) unstable; urgency=low

  * New maintainer - closes: #201371
  * New upstream release - closes: #199496
  * Added several Build-Depends - closes: #190496
  * Applied ng-1.4.4-replace_bug.patch (thus it's p1).
  * Bumped Standards-Version to 3.5.10.
  * Some tweaks in debian/rules.

 -- Masayuki Hatta <mhatta@debian.org>  Wed, 16 Jul 2003 00:15:55 +0900

ng (1.4.3.1-2) unstable; urgency=high

  * debian/control: the description of ng-cjk lacks the final newline. FIXED.

 -- Yasuhiro Take <take@debian.org>  Fri, 27 Apr 2001 10:28:42 +0900

ng (1.4.3.1-1) unstable; urgency=low

  * New upstream release

 -- Yasuhiro Take <take@debian.org>  Tue, 20 Feb 2001 23:38:34 +0900

ng (1.4.2.1-1) unstable; urgency=low

  * New upstream release

 -- Yasuhiro Take <take@debian.org>  Thu, 14 Dec 2000 12:48:24 +0900

ng (1.4.1-1) unstable; urgency=low

  * Initial Release.

 -- Yasuhiro Take <take@debian.org>  Mon, 30 Oct 2000 17:30:22 +0900

Local variables:
mode: debian-changelog
End:
